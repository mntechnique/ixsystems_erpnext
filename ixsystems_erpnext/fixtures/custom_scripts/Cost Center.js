frappe.ui.form.on("Cost Center", {
	refresh: function(frm){
		var doc = frm.doc;
		var me = this;
		if(!doc.__islocal) {
			frm.add_custom_button(__("Update Cost Center Number"), function() {
				show_cost_center_number_dialog(cur_frm);
			});
		}
	}
});

function show_cost_center_number_dialog(frm){
	var d = new frappe.ui.Dialog({
		title: __('Update Cost Center Number'),
		fields: [
			{
				"label": "Cost Center Number",
				"fieldname": "cost_center_number",
				"fieldtype": "Data",
				"reqd": 1
			}
		]
	});

	d.set_primary_action(__("Update"), function() {
		var data = d.get_values();
		frappe.call({
			method: "ixsystems_erpnext.api.update_cost_center_number",
			args: {
				"cost_center": cur_frm.doc.name,
				"dialog_values": data,
			},
			callback: function(r) {
				if(!r.exc) {
					if(r.message) {
						frappe.set_route("Form", "Cost Center", r.message);
					} else {
						frm.set_value("ix_cost_center_number", data.cost_center_number);
					}
					d.clear();
					d.hide();
				}
			}
		});
	});
	d.show();
}
