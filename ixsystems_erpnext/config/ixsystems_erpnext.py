from frappe import _

def get_data():
	return [
		{
			"label": _("Accounts"),
			"icon": "icon-star",
			"items": [
				{
					"type": "page",
					"name": "ix-chart-of-accounts",
					"label": "IX Chart of Accounts",
					"description": _("Chart of Accounts for editing Account no"),
				},
				{
					"type": "report",
					"name": "IX Trial Balance",
					"is_query_report": True,
					"doctype": "GL Entry"
				}
			]
		},
		{
			"label": _("Settings"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "IX Settings",
					"label": "IX Settings",
					"description": _("Tool for settings."),
				},
			]
		}
	]
