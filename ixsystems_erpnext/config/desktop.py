# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Ixsystems Erpnext",
			"color": "#489FDC",
			"icon": "fa fa-server",
			"type": "module",
			"label": _("Ixsystems Erpnext")
		}
	]
