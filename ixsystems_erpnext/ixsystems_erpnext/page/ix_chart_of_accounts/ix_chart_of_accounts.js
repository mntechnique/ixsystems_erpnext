//frappe.provide("frappe.treeview_settings");
frappe.provide('frappe.views.trees');

// var global_wrapper;

frappe.pages['ix-chart-of-accounts'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'IX Chart of Accounts',
		single_column: true,
	});

	frappe.model.with_doctype("Account", function() {
		var options = {
			doctype: "Account",
			meta: frappe.get_meta("Account"),
			"breadcrumb":"Ixsystems Erpnext",
			"get_tree_nodes": "ixsystems_erpnext.ixsystems_erpnext.page.ix_chart_of_accounts.ix_chart_of_accounts.get_children",
			"doctype":"Account",
			"root_label":"Accounts",
			ignore_fields:["parent_account"],
			filters: [{
				fieldname: "company",
				fieldtype:"Select",
				options: $.map(locals[':Company'], function(c) { return c.name; }).sort(),
				label: __("Company"),
				default: frappe.defaults.get_default('company') ? frappe.defaults.get_default('company'): ""
			}],
			toolbar:[
					{
						label:__("Edit Ac no"),
						condition: function(node) {
							return !node.root && !node.parent_node.root && frappe.boot.user.can_read.indexOf("Account");
						},
						click: function(node) {		
							var dialog = new frappe.ui.Dialog({
								title: __("Edit Account No"),
								fields: [
									{fieldtype: "Data", fieldname: "ac_no", label: __("Account Number")}
								]
							});

							frappe.db.get_value("Account", {"name": node.label}, "ix_ac_no",function(r) {
						    	dialog.fields_dict.ac_no.set_input(r.ix_ac_no);
						   	})

							dialog.set_primary_action(__("Save"), function() {
								var btn = this;
								var values = dialog.get_values();
								frappe.call({
									method: "ixsystems_erpnext.ixsystems_erpnext.page.ix_chart_of_accounts.ix_chart_of_accounts.save_ac_no",
									args: {
										"docname": node.label,
										"values": values
									},
									callback: function(r) {
										frappe.show_alert(r.message);
										node.parent_node.reload();
										// node.tree.selected_node.reload_parent();
										dialog.clear(); dialog.hide();
										// global_wrapper.make_tree();
									}
								});
							});
							dialog.show();
						}
					}
				// {
				// 	label: __("Foobar"),
				// 	condition: function(node) {
				// 		console.log("NODE",node);
				// 		console.log("THIS",this);
				// 		return !node.root && !node.parent_node.root && frappe.boot.user.can_read.indexOf("Account");
				// 	},
				// 	click: function(){ 
				// 		msgprint("Foobar");
				// 	}
				// }
			],
			extend_toolbar: true,
			get_tree_root: false,
			fields: [
			{fieldtype:'Data', fieldname:'account_name', label:__('New Account Name'), reqd:true,
				description: __("Name of new Account. Note: Please don't create accounts for Customers and Suppliers")},
			{fieldtype:'Data', fieldname:'ix_ac_no', label:__('Account Number'), reqd:true},
			{fieldtype:'Check', fieldname:'is_group', label:__('Is Group'),
				description: __('Further accounts can be made under Groups, but entries can be made against non-Groups')},
			{fieldtype:'Select', fieldname:'root_type', label:__('Root Type'),
				options: ['Asset', 'Liability', 'Equity', 'Income', 'Expense'].join('\n'),
				depends_on: 'eval:doc.is_group && !doc.parent_account'},
			{fieldtype:'Select', fieldname:'account_type', label:__('Account Type'),
				options: ['', 'Accumulated Depreciation', 'Bank', 'Cash', 'Chargeable', 'Cost of Goods Sold', 'Depreciation',
					'Equity', 'Expense Account', 'Expenses Included In Valuation', 'Fixed Asset', 'Income Account', 'Payable', 'Receivable',
					'Round Off', 'Stock', 'Stock Adjustment', 'Stock Received But Not Billed', 'Tax', 'Temporary'].join('\n'),
				description: __("Optional. This setting will be used to filter in various transactions.")
			},
			{fieldtype:'Float', fieldname:'tax_rate', label:__('Tax Rate'),
				depends_on: 'eval:doc.is_group==0&&doc.account_type=="Tax"'},
			{fieldtype:'Link', fieldname:'warehouse', label:__('Warehouse'), options:"Warehouse",
				depends_on: 'eval:(!doc.is_group&&doc.account_type=="Stock")',
				get_query: function() {
					return {
						filters:{
							"company": frappe.treeview_settings.filters["company"]
						}
					}
				}
			},
			{fieldtype:'Link', fieldname:'account_currency', label:__('Currency'), options:"Currency",
				description: __("Optional. Sets company's default currency, if not specified.")}
			],
			onrender: function(node) {
				var dr_or_cr = node.data.balance < 0 ? "Cr" : "Dr";
				if (node.data && node.data.balance!==undefined) {
					$('<span class="balance-area pull-right text-muted small">'
						+ (node.data.balance_in_account_currency ?
							(format_currency(Math.abs(node.data.balance_in_account_currency),
								node.data.account_currency) + " / ") : "")
						+ format_currency(Math.abs(node.data.balance), node.data.company_currency)
						+ " " + dr_or_cr
						+ '</span>').insertBefore(node.$ul);
				}
			}
		}
		
		new frappe.views.TreeView(options);
	});
	
	// wrapper.page.add_menu_item(__('Refresh'), function() {
	// 	wrapper.make_tree();
	// });

	// wrapper.make_tree = function() {
	// 	var ctype = frappe.get_route()[1] || 'Account';
		
	// 	return frappe.call({
	// 		method: 'ixsystems_erpnext.ixsystems_erpnext.page.ix_chart_of_accounts.ix_chart_of_accounts.get_children',
	// 		args: {
	// 			ctype: ctype,
	// 			doctype: "Account",
	// 			company: frappe.defaults.get_default('company'),
	// 			parent: "Accounts"
	// 		},
	// 		callback: function(r) {

	// 			// console.log('get_children', r.message);

	// 			var root = "Accounts";
	// 			frappe.tree_chart = new frappe.TreeChart(ctype, root, page,
	// 				page.main.css({
	// 					"min-height": "300px",
	// 					"padding-bottom": "25px"
	// 				}));
	// 		}
	// 	});
	// }
	// global_wrapper = wrapper;
	// wrapper.make_tree();
}

frappe.TreeChart = Class.extend({
	init: function(ctype, root, page, parent) {
		$(parent).empty();
		var me = this;
		me.ctype = ctype;
		me.page = page;
		me.can_read = frappe.model.can_read(this.ctype);
		me.can_create = frappe.boot.user.can_create.indexOf(this.ctype) !== -1 ||
					frappe.boot.user.in_create.indexOf(this.ctype) !== -1;
		me.can_write = frappe.model.can_write(this.ctype);
		me.can_delete = frappe.model.can_delete(this.ctype);

		me.page.set_primary_action(__("New"), function() {
			me.new_node();
		}, "octicon octicon-plus");

		this.tree = new frappe.ui.Tree({
			parent: $(parent),
			label: __(root),
			args: {
				ctype: ctype,
				doctype: "Account",
				company: frappe.defaults.get_default('company')
			},
			method: 'ixsystems_erpnext.ixsystems_erpnext.page.ix_chart_of_accounts.ix_chart_of_accounts.get_children',
			toolbar: [
					{toggle_btn: true},
					{
						label:__("Edit Ac no"),
						condition: function(node) {
							// console.log(node);
							return !node.root && !node.parent_node.root && me.can_read;
						},
						click: function(node) {		
							var dialog = new frappe.ui.Dialog({
								title: __("Edit Account No"),
								fields: [
									{fieldtype: "Data", fieldname: "ac_no", label: __("Account Number")}
								]
							});

							frappe.db.get_value("Account", {"name": node.label}, "ix_ac_no",function(r) {
						    	dialog.fields_dict.ac_no.set_input(r.ix_ac_no);
						   	})

							dialog.set_primary_action(__("Save"), function() {
								var btn = this;
								var values = dialog.get_values();
								frappe.call({
									method: "ixsystems_erpnext.ixsystems_erpnext.page.ix_chart_of_accounts.ix_chart_of_accounts.save_ac_no",
									args: {
										"docname": node.label,
										"values": values
									},
									callback: function(r) {
										frappe.show_alert(r.message);
										node.parent_node.reload();
										// node.tree.selected_node.reload_parent();
										dialog.clear(); dialog.hide();
										// global_wrapper.make_tree();
									}
								});
							});
							dialog.show();
						}
					},
					{
						label:__("Edit"),
						condition: function(node) {
							return !node.root && me.can_read;
						},
						click: function(node) {
							
							frappe.set_route("Form", me.ctype, node.label);
						}
					},
					{
						label:__("Add Child"),
						condition: function(node) { return me.can_create && node.expandable; },
						click: function(node) {

							me.new_node();
						},
						btnClass: "hidden-xs"
					},
					{
						label:__("Rename"),
						condition: function(node) { return !node.root && me.can_write; },
						click: function(node) {
							frappe.model.rename_doc(me.ctype, node.label, function(new_name) {
								node.$a.html(new_name);
							});
						},
						btnClass: "hidden-xs"
					},
					{
						label:__("Delete"),
						condition: function(node) { return !node.root && me.can_delete; },
						click: function(node) {
							frappe.model.delete_doc(me.ctype, node.label, function() {
								node.parent.remove();
							});
						},
						btnClass: "hidden-xs"
					}
				],
				onrender: function(node) {
					var dr_or_cr = node.data.balance < 0 ? "Cr" : "Dr";
					if (node.data && node.data.balance!==undefined) {
						$('<span class="balance-area pull-right text-muted small">'
							+ (node.data.balance_in_account_currency ?
								(format_currency(Math.abs(node.data.balance_in_account_currency),
									node.data.account_currency) + " / ") : "")
							+ format_currency(Math.abs(node.data.balance), node.data.company_currency)
							+ " " + dr_or_cr
							+ '</span>').insertBefore(node.$ul);
					}
				}
		});

		// To Expand all nodes
		// me.tree.rootnode.load_all();		
	},

	new_node: function() {
		var me = this;
		var node = me.tree.get_selected_node();

		if(!(node && node.expandable)) {
			frappe.msgprint(__("Select a group node first."));
			return;
		}

		fields = [
			{fieldtype:'Data', fieldname:'account_name', label:__('New Account Name'), reqd:true,
				description: __("Name of new Account. Note: Please don't create accounts for Customers and Suppliers")},
			{fieldtype:'Data', fieldname:'ix_ac_no', label:__('Account Number'), reqd:true},
			{fieldtype:'Check', fieldname:'is_group', label:__('Is Group'),
				description: __('Further accounts can be made under Groups, but entries can be made against non-Groups')},
			{fieldtype:'Select', fieldname:'root_type', label:__('Root Type'),
				options: ['Asset', 'Liability', 'Equity', 'Income', 'Expense'].join('\n'),
				depends_on: 'eval:doc.is_group && !doc.parent_account'},
			{fieldtype:'Select', fieldname:'account_type', label:__('Account Type'),
				options: ['', 'Accumulated Depreciation', 'Bank', 'Cash', 'Chargeable', 'Cost of Goods Sold', 'Depreciation',
					'Equity', 'Expense Account', 'Expenses Included In Valuation', 'Fixed Asset', 'Income Account', 'Payable', 'Receivable',
					'Round Off', 'Stock', 'Stock Adjustment', 'Stock Received But Not Billed', 'Tax', 'Temporary'].join('\n'),
				description: __("Optional. This setting will be used to filter in various transactions.")
			},
			{fieldtype:'Float', fieldname:'tax_rate', label:__('Tax Rate'),
				depends_on: 'eval:doc.is_group==0&&doc.account_type=="Tax"'},
			{fieldtype:'Link', fieldname:'warehouse', label:__('Warehouse'), options:"Warehouse",
				depends_on: 'eval:(!doc.is_group&&doc.account_type=="Stock")',
				get_query: function() {
					return {
						filters:{
							"company": frappe.treeview_settings.filters["company"]
						}
					}
				}
			},
			{fieldtype:'Link', fieldname:'account_currency', label:__('Currency'), options:"Currency",
				description: __("Optional. Sets company's default currency, if not specified.")}
		]

		// the dialog
		var d = new frappe.ui.Dialog({
			title: __('New {0}',[__(me.ctype)]),
			fields: fields
		})

		d.set_value("is_group", 0);
		d.set_value("root_type", node.parent_node.data.root_type)
		// create
		d.set_primary_action(__("Create New"), function() {
			var btn = this;
			var v = d.get_values();
			if(!v) return;

			var node = me.tree.get_selected_node();

			v.parent = node.label;
			v.ctype = me.ctype;

			return frappe.call({
				method: 'erpnext.accounts.utils.add_ac',
				args: v,
				callback: function(r) {
					if(!r.exc) {
						d.hide();
						if(node.expanded) {
							node.toggle_node();
						}
						node.reload();
					}
				}
			});
		});
		d.show();
	},
});

(function(){
	var oldMakeIcon=frappe.ui.TreeNode.prototype.make_icon;
	frappe.ui.TreeNode.prototype.make_icon=extendedMakeIcon;
	function extendedMakeIcon() {
		var me= this;
		var icon_html = '<i class="octicon octicon-primitive-dot text-extra-muted"></i>';
		if(this.expandable) {
			icon_html = '<i class="fa fa-fw fa-folder text-muted" style="font-size: 14px;"></i>';
		}

		if(this.data.label_acc_no){
			$(icon_html + ' <a class="tree-label grey h6">' + '<span>'
					+ (this.data.label_acc_no) + " - "
					+ '</span>' + this.get_label() + "</a>").
				appendTo(this.tree_link);
		}
		else{
			$(icon_html + ' <a class="tree-label grey h6">' + this.get_label() + "</a>").
				appendTo(this.tree_link);	
		}	

		this.tree_link.find('i').click(function() {
			setTimeout(function() { me.toolbar.find(".btn-expand").click(); }, 100);
		});

		this.tree_link.find('a').click(function() {
			if(!me.expanded) setTimeout(function() { me.toolbar.find(".btn-expand").click(); }, 100);
		});

	}
})();