from __future__ import unicode_literals
import frappe, json
from frappe.model.document import Document
from erpnext.accounts.utils import get_children as get_children_erpnext

from erpnext.accounts.doctype.account.chart_of_accounts.chart_of_accounts import get_account_tree_from_existing_company

@frappe.whitelist()
def get_children():
	acc = get_children_erpnext()

	# doctype = frappe.local.form_dict.get('doctype') or 'Account'
	# company = frappe.local.form_dict.get('company') or frappe.defaults.get_defaults().get("company")

	# parent_field = 'parent_' + doctype.lower().replace(' ', '_')
	# parent = frappe.form_dict.get("parent") or ""

	# if parent == "Accounts":
	# 	parent = ""

	# accounts = frappe.db.sql("""select name as value,
	# 	is_group as expandable
	# 	from `tab{doctype}`
	# 	where docstatus < 2
	# 	and ifnull(`{parent_field}`,'') = %s
	# 	and (`company` = %s or company is null or company = '')
	# 	order by name""".format(doctype=frappe.db.escape(doctype),
	# 	parent_field=frappe.db.escape(parent_field)), (parent, company), as_dict=1)

	# return warehouses
	# for wh in warehouses:
	# 	wh["balance"] = get_stock_value_on(warehouse=wh.value)

	for i in acc:
		acc_no = frappe.get_value("Account", i.get("value"), "ix_ac_no") or "00" 
		acc_name = frappe.get_value("Account", i.get("value"), "name")
		
		i["label_acc_no"] = acc_no

	return acc

	# company = frappe.defaults.get_defaults().get("company")
	# return get_account_tree_from_existing_company(company)
	# 

@frappe.whitelist()
def save_ac_no(docname, values):
	values = json.loads(values)

	old_value = frappe.db.get_value("Account", {"name": docname}, "ix_ac_no")

	try:
		frappe.db.set_value("Account", {"name": docname}, "ix_ac_no", values.get("ac_no"))
		frappe.db.commit()
		edited_value = frappe.db.get_value("Account", {"name": docname}, "ix_ac_no")
		if not old_value:	
			msg = "Account number edited successfully"
		else:
			msg = "Account number edited for <b>" + docname + "</b> from <b>" + old_value + "</b> to <b>" + edited_value + "</b>"

	except Exception as e:
		msg = e

	return msg