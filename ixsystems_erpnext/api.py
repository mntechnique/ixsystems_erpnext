import frappe 
import json

@frappe.whitelist()
def update_cost_center_number(cost_center, dialog_values):
	dialog_values = json.loads(dialog_values)
	new_number = dialog_values.get("cost_center_number")

	old_name_list = cost_center.split(" - ")
	new_name = ""

	if old_name_list[0].isdigit():
		old_name_list[0] = new_number
		new_name = ' - '.join(old_name_list)
	else:
		new_name = "{0} - {1}".format(new_number, cost_center)

	frappe.rename_doc("Cost Center", cost_center, new_name)
	frappe.db.commit()

	frappe.db.set_value("Cost Center", new_name, "ix_cost_center_number", new_number)
	return new_name

def cost_center_after_rename(doctype, method, olddn, newdn, merge):
	new_name_parts = newdn.split(" - ")

	if new_name_parts[0].isdigit():
		frappe.db.set_value("Cost Center", newdn, "ix_cost_center_number", new_name_parts[0])
	else:
		frappe.db.set_value("Cost Center", newdn, "ix_cost_center_number", None)

	frappe.db.commit()
	